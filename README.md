#Installation
```
mkdir Labbook
cd Labbook
git clone https://guma44@bitbucket.org/guma44/labbook.git
mkvirtualenv labbook
workon labbook
pip install -r labbook/requirements.txt
pip install south
cd labbook
python manage.py createdb

```
For the last one: make superuser and fake initial migrations
```
python manage.py syncdb
python manage.py runserver [ip_address:port eg. 131.152.25.12:7776]

```

#Settings

In webbrowser type: ip_address:port in the case you did not specified anything
type localhost:8000 or whatever appered in after the start in the line:

-> Starting development server at ...

Go to localhost:8000/admin
In Content, Pages add two link pages:
 - to "/" ie. home: name - Home, URL - /
 - to "admin" ie. admin page: name - Admin, URL - /admin

Now you can easily add notebooks and projects.
