from __future__ import unicode_literals
from future.builtins import str
from future.builtins import int
from calendar import month_name

from django.http import Http404
from django.shortcuts import get_object_or_404, render

from nbviewer.models import Notebook, Project
from mezzanine.conf import settings
from mezzanine.generic.models import Keyword
from mezzanine.utils.views import render, paginate
from mezzanine.utils.models import get_user_model

User = get_user_model()

# Create your views here.

def notebook_detail(request, slug, year=None, month=None, day=None,
                     template="notebook_detail.html"):
    print "Netbook detail"
    notebooks = Notebook.objects.published(
                                     for_user=request.user).select_related()
    notebook = get_object_or_404(notebooks, slug=slug)
    context = {"notebook": notebook, "editable_obj": notebook}
    templates = [u"notebook/notebook_detail_%s.html" % str(slug), template]
    return render(request, templates, context)

def notebook_list(request, tag=None, year=None, month=None, username=None,
                   project=None, template="notebook_list.html"):
    print "Notebook list"
    settings.use_editable()
    templates = []
    notebooks = Notebook.objects.published(for_user=request.user)
    if tag is not None:
        tag = get_object_or_404(Keyword, slug=tag)
        notebooks = notebooks.filter(keywords__keyword=tag)
    if year is not None:
        notebooks = notebooks.filter(publish_date__year=year)
        if month is not None:
            notebooks = notebooks.filter(publish_date__month=month)
            try:
                month = month_name[int(month)]
            except IndexError:
                raise Http404()
    if project is not None:
        project = get_object_or_404(Project, slug=project)
        notebooks = notebooks.filter(projects=project)
        templates.append(u"notebook/notebook_list_%s.html" %
                          str(project.slug))
    author = None
    if username is not None:
        author = get_object_or_404(User, username=username)
        notebooks = notebooks.filter(user=author)
        templates.append(u"notebook/notebook_list_%s.html" % username)

    prefetch = ("projects", "keywords__keyword")
    notebooks = notebooks.select_related("user").prefetch_related(*prefetch)
    notebooks = paginate(notebooks, request.GET.get("page", 1),
                          settings.NOTEBOOKS_PER_PAGE,
                          settings.MAX_PAGING_LINKS)
    context = {"notebooks": notebooks, "year": year, "month": month,
               "tag": tag, "project": project, "author": author}
    templates.append(template)
    return render(request, templates, context)
