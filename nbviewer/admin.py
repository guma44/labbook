from __future__ import unicode_literals

from copy import deepcopy

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import Notebook, Project
from mezzanine.conf import settings
from mezzanine.core.admin import DisplayableAdmin, OwnableAdmin
# Register your models here.


notebook_fieldsets = deepcopy(DisplayableAdmin.fieldsets)
notebook_fieldsets[0][1]["fields"].insert(1, "projects")
notebook_fieldsets[0][1]["fields"].extend(["path_to_notebook", "allow_comments"])
notebook_list_display = ["title", "user", "status", "admin_link"]
notebook_fieldsets = list(notebook_fieldsets)
notebook_fieldsets.insert(1, (_("Other notebooks"), {
    "classes": ("collapse-closed",),
    "fields": ("related_notebooks",)}))
notebook_list_filter = deepcopy(DisplayableAdmin.list_filter) + ("projects",)


class NotebookAdmin(DisplayableAdmin, OwnableAdmin):
    """
    Admin class for notebooks.
    """

    fieldsets = notebook_fieldsets
    list_display = notebook_list_display
    list_filter = notebook_list_filter
    filter_horizontal = ("projects", "related_notebooks",)

    def save_form(self, request, form, change):
        """
        Super class ordering is important here - user must get saved first.
        """
        OwnableAdmin.save_form(self, request, form, change)
        return DisplayableAdmin.save_form(self, request, form, change)


class ProjectAdmin(admin.ModelAdmin):
    """
    Admin class for projects. Hides itself from the admin menu
    unless explicitly specified.
    """

    fieldsets = ((None, {"fields": ("title","content")}),)

admin.site.register(Notebook, NotebookAdmin)
admin.site.register(Project, ProjectAdmin)

