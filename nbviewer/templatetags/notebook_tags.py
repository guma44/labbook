from __future__ import unicode_literals
from datetime import datetime

from django.db.models import Count, Q

from mezzanine.generic.models import Keyword
from mezzanine import template
from mezzanine.utils.models import get_user_model
from nbviewer.models import Notebook, Project

User = get_user_model()

register = template.Library()


@register.as_tag
def notebook_months(*args):
    """
    Put a list of dates for notebook posts into the template context.
    """
    dates = Notebook.objects.published().order_by('-publish_date').values_list("publish_date", flat=True)
    date_dicts = [{"date": datetime(d.year, d.month, 1)} for d in dates]
    month_dicts = []
    for date_dict in date_dicts:
        if date_dict not in month_dicts:
            month_dicts.append(date_dict)
    for i, date_dict in enumerate(month_dicts):
        month_dicts[i]["post_count"] = date_dicts.count(date_dict)
    return month_dicts


@register.as_tag
def notebook_projects(*args):
    """
    Put a list of projects for notebook posts into the template context.
    """
    posts = Notebook.objects.published()
    projects = Project.objects.filter(notebooks__in=posts)
    return list(projects.annotate(post_count=Count("notebooks")))


@register.as_tag
def notebook_authors(*args):
    """
    Put a list of authors (users) for notebook posts into the template context.
    """
    notebook_posts = Notebook.objects.published()
    authors = User.objects.filter(notebooks__in=notebook_posts)
    return list(authors.annotate(post_count=Count("notebooks")))


@register.as_tag
def notebook_recent_posts(limit=5, tag=None, username=None, project=None):
    """
    Put a list of recently modified notebook posts into the template
    context. A tag title or slug, project title or slug or author's
    username can also be specified to filter the recent posts returned.

    Usage::

        {% notebook_recent_posts 5 as recent_posts %}
        {% notebook_recent_posts limit=5 tag="django" as recent_posts %}
        {% notebook_recent_posts limit=5 project="python" as recent_posts %}
        {% notebook_recent_posts 5 username=admin as recent_posts %}

    """
    notebook_posts = Notebook.objects.published().select_related("user")
    title_or_slug = lambda s: Q(title=s) | Q(slug=s)
    if tag is not None:
        try:
            tag = Keyword.objects.get(title_or_slug(tag))
            notebook_posts = notebook_posts.filter(keywords__keyword=tag)
        except Keyword.DoesNotExist:
            return []
    if project is not None:
        try:
            project = Project.objects.get(title_or_slug(project))
            notebook_posts = notebook_posts.filter(projects=project)
        except Project.DoesNotExist:
            return []
    if username is not None:
        try:
            author = User.objects.get(username=username)
            notebook_posts = notebook_posts.filter(user=author)
        except User.DoesNotExist:
            return []
    return list(notebook_posts[:limit])

@register.as_tag
def notebook_recently_added_posts(limit=5, tag=None, username=None, project=None):
    """
    Put a list of recently published notebook posts into the template
    context. A tag title or slug, project title or slug or author's
    username can also be specified to filter the recent posts returned.

    Usage::

        {% notebook_recent_posts 5 as recent_posts %}
        {% notebook_recent_posts limit=5 tag="django" as recent_posts %}
        {% notebook_recent_posts limit=5 project="python" as recent_posts %}
        {% notebook_recent_posts 5 username=admin as recent_posts %}

    """
    notebook_posts = Notebook.objects.published().order_by("-publish_date").select_related("user")
    title_or_slug = lambda s: Q(title=s) | Q(slug=s)
    if tag is not None:
        try:
            tag = Keyword.objects.get(title_or_slug(tag))
            notebook_posts = notebook_posts.filter(keywords__keyword=tag)
        except Keyword.DoesNotExist:
            return []
    if project is not None:
        try:
            project = Project.objects.get(title_or_slug(project))
            notebook_posts = notebook_posts.filter(projects=project)
        except Project.DoesNotExist:
            return []
    if username is not None:
        try:
            author = User.objects.get(username=username)
            notebook_posts = notebook_posts.filter(user=author)
        except User.DoesNotExist:
            return []
    return list(notebook_posts[:limit])


@register.inclusion_tag("admin/includes/quick_notebook.html", takes_context=True)
def quick_notebook(context):
    """
    Admin dashboard tag for the quick notebook form.
    """
    context["form"] = NotebookForm()
    return context
