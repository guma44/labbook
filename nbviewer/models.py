from __future__ import unicode_literals
import re
import os
from future.builtins import str

from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from mezzanine.conf import settings
from mezzanine.core.models import Displayable, Ownable, RichText, Slugged, RichTextField
from mezzanine.generic.fields import CommentsField
from mezzanine.utils.models import AdminThumbMixin
from mezzanine.utils.html import TagCloser
from django.template.defaultfilters import truncatewords_html

import IPython.nbformat as nbformat
from IPython.nbconvert.exporters import HTMLExporter
from IPython.config import Config


# Create your models here.
#
class Project(RichText, Slugged):

    class Meta:
        verbose_name = _("Project")
        verbose_name_plural = _("Projects")
        ordering = ("title",)


class Notebook(Displayable, Ownable, RichText, AdminThumbMixin):
    projects = models.ManyToManyField("Project",
                                      verbose_name=("Projects"),
                                      blank=True,
                                      related_name="notebooks")
    path_to_notebook = models.CharField(max_length=1000)
    comments = CommentsField(verbose_name=_("Comments"))
    allow_comments = models.BooleanField(verbose_name=_("Allow comments"),
                                         default=True)
    related_notebooks = models.ManyToManyField("self",
                                           verbose_name=_("Related notebooks"), blank=True)
    date_modified = models.DateTimeField(auto_now=True, blank=True)

    def get_absolute_url(self):
        """
        URLs for blog posts can either be just their slug, or prefixed
        with a portion of the post's publish date, controlled by the
        setting ``BLOG_URLS_DATE_FORMAT``, which can contain the value
        ``year``, ``month``, or ``day``. Each of these maps to the name
        of the corresponding urlpattern, and if defined, we loop through
        each of these and build up the kwargs for the correct urlpattern.
        The order which we loop through them is important, since the
        order goes from least granualr (just year) to most granular
        (year/month/day).
        """
        url_name = "notebook_detail"
        kwargs = {"slug": self.slug}
        return reverse(url_name, kwargs=kwargs)

    def get_notebook_body(self):
        # Instantiate and configure the exporter
        c = Config()
        c.HTMLExporter.template_file = 'full'
        c.NbconvertApp.fileext = 'html'
        c.CSSHTMLHeaderTransformer.enabled = False
        # don't strip the http prefix - we use it for redirects
        c.Exporter.filters = {'strip_files_prefix': lambda s: s}
        html_exporter = HTMLExporter(config=c)

        with open(self.path_to_notebook) as f:
            nb = nbformat.reads(f.read(), 4)
        body = re.sub(r"<style>.*style>",'', html_exporter.from_notebook_node(nb)[0], flags=re.DOTALL)
        body = re.sub(r"<style.*style>",'', body, flags=re.DOTALL)
        body = re.sub(r"class=\"container\"(\s+id=\"notebook-container\")", r'\1', body, flags=re.DOTALL)
        return body

    def save(self, *args, **kwargs):
        """Save model. If model exists and save function is called again, it will be
        updated - save function with extract the body and save it again"""
        self.content = self.get_notebook_body()
        super(Notebook, self).save(*args, **kwargs)

    def description_from_content(self):
        return self.path_to_notebook
        # description = self.content
        # # Strip everything after the first block or sentence.
        # description = truncatewords_html(description, 50)
        # return description
        #
    def clean(self):
        super(Notebook, self).clean()
        if not os.path.exists(self.path_to_notebook):
            raise ValidationError(_("Path %(path)s does not exist"),
                                  params={'path': self.path_to_notebook})



    class Meta:
        verbose_name = _("Notebook")
        verbose_name_plural = _("Notebooks")
        ordering = ("-date_modified",)


