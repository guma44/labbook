from __future__ import unicode_literals

from django.conf.urls import patterns, url

from mezzanine.conf import settings

from .views import notebook_list


# Leading and trailing slahes for urlpatterns based on setup.
NOTEBOOK_SLUG = 'notebook'
_slashes = (
    "notebook/",
    "",
)

# Notebook patterns.
urlpatterns = patterns("nbviewer.views",
    url("^%stag/(?P<tag>.*)%s$" % _slashes, "notebook_list",
        name="notebook_list_tag"),
    url("^%sproject/(?P<project>.*)%s$" % _slashes,
        "notebook_list", name="notebook_list_project"),
    url("^%sauthor/(?P<username>.*)%s$" % _slashes,
        "notebook_list", name="notebook_list_author"),
    url("^%sarchive/(?P<year>\d{4})/(?P<month>\d{1,2})%s$" % _slashes,
        "notebook_list", name="notebook_list_month"),
    url("^%sarchive/(?P<year>\d{4})%s$" % _slashes,
        "notebook_list", name="notebook_list_year"),
    url("^%s(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/"
        "(?P<slug>.*)%s$" % _slashes,
        "notebook_detail", name="notebook_detail_day"),
    url("^%s(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<slug>.*)%s$" % _slashes,
        "notebook_detail", name="notebook_detail_month"),
    url("^%s(?P<year>\d{4})/(?P<slug>.*)%s$" % _slashes,
        "notebook_detail", name="notebook_detail_year"),
    url("^%s(?P<slug>.*)%s$" % _slashes, "notebook_detail",
        name="notebook_detail"),
    url("^notebook/$", "notebook_list", name="notebook_list"),
)

